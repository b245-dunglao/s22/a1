// console.log("Hello Batch 245");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    function register(info){
    	let checkUser = registeredUsers.includes(info);
    		if (checkUser === true){
    			alert("Registration failed. Username already exists!");
    		}
    		else {
    			registeredUsers.push(info);
    			alert("Thank you for registering!");
    		}
    }

    register("Conan O' Brien");
    console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

	function addFriend(info) {
		let checkUser = registeredUsers.includes(info);
			if (checkUser === true){
				friendsList.push(info);
				alert("You have added " + info + " as a friend!");
			}
			else {
				alert("User not found.")
			}
	}

	addFriend("Akiko Yukihime");
	console.log(friendsList);


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

	function viewFriendsList() {
		let checkFriendList = friendsList.filter(function(userInfo){
			return (userInfo != "");
		});

		if (checkFriendList == 0 ){
			alert("You currently have 0 friends. Add one first.");
		}
		else {
			console.log(friendsList.join("\n"));;
		}

	}
    
    viewFriendsList();

    // to mimic the sample output
    addFriend("Conan O' Brien");
    viewFriendsList();
    // added unregistered friend to check if function is working
    addFriend("James Gunn");


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/	

	function displayFriends() {
		let checkFriendList = friendsList.filter(function(userInfo){
			return (userInfo != "");
		});
		if (checkFriendList == 0 ){
			alert("You currently have 0 friends. Add one first.");
		}
		else {
			console.log("You currently have " + checkFriendList.length + " friends");
		}
	}

	displayFriends();


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

	function deleteFriend(){
		let checkFriendList = friendsList.filter(function(userInfo){
			return (userInfo != "");
		});

		if (checkFriendList == 0 ){
			alert("You currently have 0 friends. Add one first.");
		}
		else {
			friendsList.pop();
		}

	}

	deleteFriend();
	console.log(friendsList);


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/	

	// added friends for sample.
	friendsList.push("Conan O' Brien","Michelle Queen", "Shane Miguelito","Fernando Dela Cruz");
	viewFriendsList();


	function deleteUsingName(info){
		friendsList.splice(friendsList.indexOf(info),1);
		viewFriendsList();
	}

	deleteUsingName("Michelle Queen");
	
	function deleteUsingIndex(info){
		friendsList.splice(info,1);
		viewFriendsList();
	}

	deleteUsingName(1);